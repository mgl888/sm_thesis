%% This is an example first chapter.  You should put chapter/appendix that you
%% write into a separate file, and add a line \include{yourfilename} to
%% main.tex, where `yourfilename.tex' is the name of the chapter/appendix file.
%% You can process specific files by typing their names in at the 
%% \files=
%% prompt when you run the file main.tex through LaTeX.
\chapter{BlueFlash Board Design}
\label{chap:board}

The BlueFlash board is the underlying flash storage device in BlueDBM. We
expect to use the platform for the next 3-4 years in the exploration of Big
Data applications at scale. Thus we needed a competitive hardware platform that
is feasible, cost effective and realistic in reflecting the state of flash
storage in the current and near future markets. In the enterprise market,
today's state of art commercial flash drives are PCIe-based with capacities
from 0.5TB to 3TB, achieving up to 2.7GB/s of bandwidth~\cite{hgst, micronSSD}. We strive for
similar performance and capacity for BlueFlash. The BlueFlash board also must
provide support for the architectural ideas in BlueDBM. This includes
sufficient reconfigurable fabric for hardware accelerator implementation,
direct serial links to other BlueFlash boards and an interface to servers via
PCIe. With these goals in mind, we discuss the design choices of the board. 

\section{Flash Board Organization and Component Choices}

Since PCIe is required for the host to access the flash board, we chose to use
an off-the-shelf Xilinx FPGA development board (VC707~\cite{vc707}) as a cost-effective way to
provide this interface. We call this board the \textit{carrier} board. In
addition the PCIe, the board provides a sizable Virtex-7 FPGA for accelerator
implementation and on-board DRAM for caching. The BlueFlash boards will be an
attachment to the carrier board via the FPGA Mezzanine Card interface
(FMC)~\cite{FMC}. The FMC connector provides high speed serial links as well as
GPIO pins directly from the carrier FPGA. This organization is shown in
Figure~\ref{fig:boardAttach}. 


\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.6]{./figures/pdf_out/3_board_attachment-crop.pdf}
	\caption{FMC Flash Board Attached to Carrier Board}
	\label{fig:boardAttach}
	\end{center}
\end{figure}

%The design of the flash board is primarily constrained by the number of I/O
%pins, cost, interface bandwidth and physical dimensions. 

Today's NAND packages are typically 64Gbits to 256Gbits in capacity. 
By themselves, the chips are relatively slow, but by accessing them in parallel,
we can aggregate to a significant amount of bandwidth. The common method to
achieve this is by arranging chips into buses sharing common data and
command signals (Figure~\ref{fig:chipBus}). There are two levels of parallelism here: 
\begin{enumerate}
	\item Bus parallelism: Buses are independent, and can be addressed in parallel. 
	\item Chip parallelism: Chips on the same bus can be accessed in an
		interleaved fashion to hide latency. For example, while one chip is busy
		performing a read, another chip can take control of the bus to stream
		back read data.  
\end{enumerate}


\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.6]{./figures/pdf_out/3_chips_on_bus-crop.pdf}
	\caption{NAND Chip Connections on a Bus}
	\label{fig:chipBus}
	\end{center}
\end{figure}

Bus parallelism is clearly preferred because buses are independent and we can
achieve almost linear bandwidth scaling this way.  However, increasing the
number of buses also significantly increases the number of I/O pins on the
controller.  We can add more chips per bus to leverage chip parallelism, but
this is only effective if the bus bandwidth has not saturated.
Table~\ref{tab:NandArrangement} shows the different ways to arrange a 512GB
flash card and the achieved theoretical performance.  Calculation is made based
on Micron MLC flash with 8-bit wide data interface and 8KB pages operating at 100MHz
DDR with read latency of 75$\mu$s and program latency of 500$\mu$s. We observe that we
need 8 to 16 buses to be competitive with modern SSDs. 

%bw table
\begin{table}[ht]
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|}\hline
			{\bf Chips Per Bus}		& {\bf Buses} 		& {\bf Pins} 		& \begin{tabular}[x]{@{}c@{}}\textbf{Read Bandwidth}\\\textbf{(MB/s)}\end{tabular} & \begin{tabular}[x]{@{}c@{}}\textbf{Write Bandwidth}\\\textbf{(MB/s)}\end{tabular} 	\\ \hline \hline
			1								& 64					& 1024				& 6667								& 1000										\\ \hline
			2								& 32					& 576					& 6400								& 1000										\\ \hline
			4								& 16					& 352					& 3200								& 1000										\\ \hline
			8								& 8					& 240					& 1600								& 1000										\\ \hline		
			16								& 4					& 184					& 800									& 800										\\ \hline		
			32								& 2					& 156					& 400									& 400										\\ \hline		
			64								& 1					& 142					& 200									& 200										\\ \hline		
		\end{tabular}
		\caption{NAND arrangements for a 512GB flash card}
		\label{tab:NandArrangement}
	\end{center}
\end{table}

One design option is to connect these NAND buses directly to the FMC connector
GPIOs, and program the flash controller on the carrier FPGA. However, the FMC has
insufficient pins and this design also wastes valuable resources on the carrier
FPGA that could be used for accelerator programming. Furthermore, because of
the relative high bus speeds of modern NAND chips, signaling across the FMC
connector could cause integrity problems. Instead, we chose to integrate a
second smaller FPGA (Xilinx Artix-7) on the flash board. This FPGA contain the
flash controller, and it communicates with the primary carrier FPGA using
multiple high speed serial links (GTP). We chose an 8-bus, 8 chips per bus
(with 4 chip dies per package), 512GB flash board design after factoring in
cost, physical dimensions and I/O pin constraints.  The specifications of the
final flash board is outlined in Table~\ref{tab:boardSpecs}, and the architecture diagram is
shown in Figure~\ref{fig:boardArch}. 

\begin{table}[ht]
	\begin{center}
		\begin{tabular}{|l|c|}\hline
			{\bf NAND Buses}				& 8 \\ \hline
			{\bf NAND Chips Per Bus} 	& 8 \\ \hline
			{\bf Total NAND Packages}	& 16 \\ \hline
			{\bf Theoretical Read Bandwidth} & 1600 MB/s \\ \hline
			{\bf Theoretical Write Bandwidth} & 1000 MB/s \\ \hline
			{\bf Flash Controller FPGA} & Xilinx Artix-7 XC7A200TFBG676 \\ \hline
			{\bf Serial GTX-GTP Connections} & \multirow{2}{*}{4 @ 6.6Gbps} \\
		 	{\bf (Controller to Carrier FPGA)} & \\ \hline
 		   {\bf Serial GTX-GTX Connections} & \multirow{2}{*}{4 @ 10Gbps} \\
			{\bf (Inter-node)} & \\ \hline
			{\bf Estimated Cost} & \$1000 \\ \hline
		\end{tabular}
		\caption{FMC Flash Board Specifications}
		\label{tab:boardSpecs}
	\end{center}
\end{table}


\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.7]{./figures/pdf_out/3_board_arch-crop.pdf}
	\caption{Flash Board Architecture}
	\label{fig:boardArch}
	\end{center}
\end{figure}

Finally, to create the sideband network directly between storage devices, the
BlueFlash board breaks out high speed serial connections (GTX) that is buried
in the FMC connector of the carrier board as SATA ports. SATA cables are
inexpensive and can run up to 1 meter in length. 

A photo of the BlueFlash board and its corresponding layout is shown in
Figure~\ref{fig:boardPic}. Note that we use 2 flash boards per node.


\begin{figure}
	\begin{center}
		\begin{subfigure}{.9\textwidth}
	\includegraphics[scale=0.14]{./figures/boardpic.jpg}
\end{subfigure}
\begin{subfigure}{.9\textwidth}
	\includegraphics[scale=0.9]{./figures/pdf_out/3_board_layout-crop.pdf}
\end{subfigure}
\caption{BlueFlash Board Hardware and Layout}
\label{fig:boardPic}
\end{center}
\end{figure}







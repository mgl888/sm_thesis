main: main.tex main.bib
	pdflatex main.tex
	bibtex main
	pdflatex main.tex
	pdflatex main.tex

.PHONY: clean

clean:
	rm -rf main.pdf main.aux main.bbl main.dvi main.blg *.log *.aux main.lof main.lot main.toc main.synctex.gz

%% This is an example first chapter.  You should put chapter/appendix that you
%% write into a separate file, and add a line \include{yourfilename} to
%% main.tex, where `yourfilename.tex' is the name of the chapter/appendix file.
%% You can process specific files by typing their names in at the 
%% \files=
%% prompt when you run the file main.tex through LaTeX.
\chapter{Introduction}
\label{chap:intro}

NAND flash has risen to become a ubiquitous storage medium in recent years.
Compared to its hard disk counterpart, flash provides orders of magnitude
better performance in both sequential and random workloads, as well as 100x
lower access latency at 4x lower power\cite{nandEval}. Furthermore, flash
density has scaled directly with transistor size and the ability to store
multiple bits per cell, quickly reaching the low cost level of hard drives at
comparable capacities.  Raw bandwidth has also increased steadily with higher
clocked interfaces and chips. 

\section{Flash in Big Data Systems}
Because of its significant performance and cost benefits, flash based storage
has found its way into modern Big Data systems. Big Data applications are
characterized by their large size, high transactional volume, and/or complex
analytical queries. Statistical analysis on
large quantities of social media data (Twitter or Facebook) to identify consumer
trends is one such example. The hardware system used today to perform such analytics typically
consists of a cluster of ARM or x86 based servers with access to hundreds of
gigabytes of RAM, and a backing store of storage area network (SAN) or RAID
array of hard disks. Given that the size of Big Data applications do not fit in
DRAM, flash and other non-volatile memories (NVMs) are often used as an additional
caching layer between RAM and hard disks. However, as the
price of NAND flash falls, it is increasingly attractive as a primary storage
device in place of hard disks. Many companies have built higher performance PCIe
flash cards and even large flash based
storage arrays~\cite{violinmemory, purestorage} to replace traditional disk
arrays. Yet, while these flash arrays can achieve several gigabytes of bandwidth
per second at ultra low latencies independently, when integrated into a full
system, the performance of the end application does not increase proportionally.
This is primarily because of other bottlenecks in the system. 

\section{Bottlenecks in Flash Storage Systems}
\label{sec:bottlenecks}
%talk about stack and pitfalls and oportunities
%http://www.anandtech.com/show/7843/testing-sata-express-with-asus/4
Today's systems are not designed with flash storage as the primary medium and
therefore introduces many layers of inefficiencies. These are highlighted
below:
\begin{itemize} 
	\item \textbf{Archaic Interfaces Standards:} Most SSDs today are SATA 3.0
		drives conforming to the Advanced Host Controller Interface (AHCI)
		standard interface \cite{AHCI} that was originally developed in 2003. Not
		only are the SSDs physically bottlenecked by the 6Gb/s maximum speed of
		SATA 3.0, AHCI is also an old standard targeted for hard drives with
		shallow queue depth and several microseconds of latency\cite{ahci_vs_nvme}.
		This is not a problem for a hard disk which is
		only capable of several hundred I/O operations per second (IOPS) at
		10-12ms seek latencies. However, flash latency is typically less than
		100us, and it thrives on deep command queues to extract parallelism.  Thus,
		AHCI is a significant overhead for SSDs. Industry has realized this and
		is addressing the problem with the introduction of the NVMe
		standard on PCIe\cite{NVMe}.
	\item \textbf{Network Latency and Overhead:} Big Data storage systems that
		require high capacity are usually constructed in two ways: (i) building a
		Storage Area Network (SAN) or (ii) using a distribute file system. In a
		SAN, large amounts of storage are placed in a storage node such as a RAID
		server, and these storage devices are connected together using a
		dedicated network (i.e. SAN), providing the abstraction of locally
		attached disk to the application servers. However, the physical storage
		network is usually Ethernet based running on protocols such as iSCSI or
		FCoE, which adds milliseconds of software and network latency~\cite{QuickSAN}. An
		alternative organization is to distribute the storage among the
		application hosts and use the general purpose network along with a
		distributed file system (e.g., NFS~\cite{nfs}, Lustre~\cite{Lustre},
		GFS~\cite{GFS}) to provide a file-level sharing abstraction. This is
		popular with distributed data processing platforms such as
		MapReduce~\cite{MapReduce}. While a distributed file system is cheap and
		scalable, the software overhead of concurrency control and the
		high-latency congestion-prone general purpose network degrades
		performance. Traditionally, these network, software and
		protocol latencies are tolerable because they are insignificant compared
		to the seek latency of hard disks, but they are significant bottlenecks for SSDs. 
	\item \textbf{File System Inefficiencies:}
		In a typical storage system, the file system is responsible for managing
		the blocks of the storage device. However, NAND flash require a different kind of management because it is inherently a
		lossy storage medium that have a limited number of program/erase cycles.
		Manufacturers have chosen to add a Flash Translation Layer (FTL) to
		perform NAND management tasks such as wear leveling, bad block management
		and garbage collection, while preserving a block view of the storage
		device to the file system. However, these two software layers operate
		independently. This introduces management redundancies, extra I/Os and
		performance degradation~\cite{REDO}. 
	\item \textbf{Processor Limitations:}
%		http://www.dell.com/downloads/global/products/pvaul/en/sql-server-perf-with-express-flash-and-poweredge-r720.pdf
		With flash drives that have 10x the sequential I/O performance and 1000x
		the random I/O performance of hard disks, the bottleneck for the user application often becomes the processor. 
		Dell reported that their PowerEdge R720 server processor utilization jumps
		from 20\% to 90\% when 24 hard disks are swapped for merely 4 PCIe SSDs when running OLTP SQL Server workloads~\cite{dell}.
		Such examples demonstrate that \textit{to fully take advantage of high performance SSDs, the
		processor-to-drive ratio must be kept high}. In turn, this means we need
		more server nodes. However, increasing the number of
		server nodes also increases network overhead, potentially degrading any performance
		gain SSDs may provide. 
\end{itemize}

These limitations thwart the full performance advantages of NAND flash, and eliminating them
require a system level overhaul. 

%talk about bluedbm platform and stack
\section{BlueDBM: A Hardware Accelerated Flash Storage Platform for Big Data} 

%platform
BlueDBM~\cite{BlueDBM} is a novel and vertically integrated flash storage
platform designed for Big Data systems. We call the system a ~\textbf{platform}
because it provides two key services to the end users:
\begin{enumerate}
	\item An ultra-low latency, high bandwidth distributed flash store 
	\item A programming platform for implementing reconfigurable application specific hardware accelerators\end{enumerate}

\subsection{BlueDBM Architecture}
The BlueDBM hardware architecture is shown in Figure~\ref{fig:bluedbmArch}. The system is
composed of a set of identical storage nodes. Each node contains terabytes of
NAND flash storage controlled and managed by reconfigurable fabrics (i.e. FPGAs),
which is connected to an x86 server via a high-speed PCIe link. The FPGAs are
directly connected to each other via multi-gigabit serial
links, thus forming a separate dedicated low latency hardware storage network.
By creating a dedicated sideband network, we avoid the overhead of Ethernet and
the software network stack. The system scales well in performance and storage by
simply adding more identical nodes. 


\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.7]{./figures/pdf_out/1_blueDBM_arch-crop.pdf}
	\caption{BlueDBM Architecture}
	\label{fig:bluedbmArch}
	\end{center}
\end{figure}

\subsection{BlueDBM Stack}
The hardware-software stack of BlueDBM is shown in Figure~\ref{fig:bluedbmStack}. BlueDBM
provides three interfaces to the user application: 
\begin{enumerate}
	\item \textbf{A distributed file system interface}: Using this interface,
		the underlying storage device appears as a unified store. Any existing
		application may operate on top of this interface without modification. To
		support this, BlueDBM runs a file system called REDO ~\cite{REDO}, which is a distributed
		flash-aware file system that works in concert with the reconfigurable
		flash controller to manage the underlying flash store.  
	\item \textbf{A block interface}: This lower level interface provides access
		to the storage blocks of the flash store. For applications that do not
		require files and manages its own data (e.g. a database), this interface
		provides a more direct path to storage with lower latency.  
	\item \textbf{A hardware-software accelerator interface}: This interface
		assists users in the integration of custom hardware accelerators into the
		system. In the hardware platform, we provide a method for \textit{user hardware accelerators} to
		have direct access to the flash store to do in-store computation. In software, we expose a proxy-stub type interface from XBSV~\cite{xbsv}. This allows
		\textit{user software} to directly communicate with the accelerator.
\end{enumerate}

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.7]{./figures/pdf_out/1_bluedbm_stack-crop.pdf}
	\caption{BlueDBM Hardware Software Stack}
	\label{fig:bluedbmStack}
	\end{center}
\end{figure}


In BlueDBM, flash management complexities are hidden from the user with the inclusion of a split hardware and software Flash Translation Layer (FTL). 
The software FTL is embedded in either the file
system or the block device driver, and it is responsible for garbage collection
and file segment mapping. The hardware FTL is a thin layer above the flash
controller that handles wear-leveling and bad block management. 

We note that BlueDBM overcomes the limitations that was mentioned in
Section\ref{sec:bottlenecks}. First, we leverage PCIe, thin XBSV drivers and a custom flash management
interface to replace SATA3 and AHCI. Second, we use low latency serial links
directly between flash devices for scaling and networking. Third, we introduce
a flash-aware file system that cooperate with the hardware controller to manage
flash. Finally, we expose simple interfaces for applications to perform
in-store hardware accelerated computation. 

\section{Improving the BlueDBM Platform Hardware}

A 4-node prototype of BlueDBM was constructed in 2012 and has shown
excellent scalability and performance potential. However, the prototype was
built using BlueSSD~\cite{BlueSSD} boards that only provided 16GB of storage at
80MB/s. In comparison, today's PCIe SSDs are typically several terabytes in
capacity with bandwidth of 1-3GB/s~\cite{hgst, micronSSD}. Therefore, to construct a modern and usable
BlueDBM platform to explore real Big Data problems at scale, we required an
improved flash card and flash controller. Flash chips have evolved
significantly since BlueSSD, and the following challenges have emerged:
\begin{enumerate} 
	\item Interface speed between NAND chips and controller have increased to
		several hundred MHz making precise signal skew adjustments and board trace
		matching a requirement.
	\item As NAND density increases and NAND cells scale down in size, stronger
		bit error correction algorithms are required.
	\item Number of buses and chips must quadruple compared to BlueSSD to reach
		terabytes of capacity and several GB/s of bandwidth.
	\item Controller must keep up with the high data rate of the new NAND chips
		for best performance.
	\item Tighter and stricter and timing requirements must be met.
\end{enumerate}

Next, we introduce BlueFlash as the new high performance flash storage
device for BlueDBM. 

\section{BlueFlash: A 0.5TB Flash Board and Controller}
\label{sec:blueflashStack}

This thesis work presents BlueFlash, a revamped and improved storage device for
the BlueDBM platform. We make the following contributions
\begin{enumerate}
	\item We present the design of a new 0.5TB flash board 
	\item We design and benchmark a new flash controller for the board capable of reaching 1.2GB/s
	\item We explore the latency, error and power characteristics of the flash chips
\end{enumerate}

The portion of the BlueDBM platform in question is highlighted in
Figure~\ref{fig:blueFlashStack}. The BlueFlash controller is situated beneath the hardware FTL
layer in the stack, thus it does not manage NAND. Instead, it exposes
command, address and data burst interfaces to access the flash chip array in a
parallel and error-free manner. Its tasks are summarized below:
\begin{enumerate}
	\item Communicate with each raw flash chip: data and command I/O, status monitoring and initialization.
	\item Calibrate timing and adjust skews
	\item Correct page bit errors
	\item Schedule commands for parallel access to multiple chips and buses
\end{enumerate}

We are in the process of constructing a 16-node BlueDBM platform using 2
BlueFlash boards per node. This provides 16TB of flash storage at approximately
38-51GB/s.

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.7]{./figures/pdf_out/1_bluedbm_blueflash-crop.pdf}
	\caption{BlueFlash in the BlueDBM Stack}
	\label{fig:blueFlashStack}
	\end{center}
\end{figure}

The rest of the thesis is organized as follows: Chapter~\ref{chap:board} describes the
flash board; Chapter~\ref{chap:controllerdesign} presents the flash controller design choices and
implementation; Chapter~\ref{chap:eval} discusses the performance results of BlueFlash
running on actual hardware; Chapter~\ref{chap:related} talks about related work;
Chapter~\ref{chap:conclusion} concludes with future work. 

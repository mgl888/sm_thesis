%% This is an example first chapter.  You should put chapter/appendix that you
%% write into a separate file, and add a line \include{yourfilename} to
%% main.tex, where `yourfilename.tex' is the name of the chapter/appendix file.
%% You can process specific files by typing their names in at the 
%% \files=
%% prompt when you run the file main.tex through LaTeX.
\chapter{BlueFlash Controller Design and Implementation}
\label{chap:controllerdesign}

As discussed in Section~\ref{sec:blueflashStack}, the BlueFlash controller is a layer beneath
the hardware FTL and its purpose is to provide a fast, parallel and error-free
interface to the flash card with minimal bandwidth and latency overhead.
Internally, the flash controller provides the protocols to communicate with
individual flash chip, performs timing adjustments, corrects bit errors, and
manages parallel accesses to multiple bus/chip. We first present the user
interface of the flash controller (as used by the hardware FTL). Then we
provide some background information on the NAND chips before discussing the
design and implementation of the controller in detail. 

\section{Controller Interface}

The flash controller interface is defined below in Bluespec: 

\begin{lstlisting}[basicstyle=\footnotesize\ttfamily, frame=single]
interface FlashIfc;       
   method Action sendCmd (FlashOp op, Bit#(4) bus, Bit#(3) chip, 
                          Bit#(16) block, Bit#(8) page, Bit#(8) tag);        
   method Action writeWord (Bit#(128) data, Bit#(8) tag);                  
   method ActionValue#(Tuple2#(Bit#(128), Bit#(8)) readWord (); 
   method ActionValue#(Bit#(8)) getWriteDataReq ();
endinterface 
\end{lstlisting}

The user of the flash controller is expected to have a multi-entry page buffer
(256 entries or greater is recommended). Each entry of the buffer can hold an
8KB flash page and has a unique tag associated with it. 
This buffer will be used as a completion buffer for page reads, and as a
write buffer to temporarily hold write data until it can be transferred. 
The operations (\textit{FlashOp}) supported by the flash controller are read page, write page and erase block. We walk through each operation below. 
\begin{itemize}
	\item Page Read: 
		\begin{enumerate}
			\item User allocates an entry in the page buffer 
			\item User sends the read request using the \textit{sendCmd} method
				along with the unique tag associated with the page buffer entry
				from Step 1.
			\item User continuously obtains 128-bit bursts of tagged read data
				using the \textit{readWord} method. Each burst should fill the
				entry of the page buffer that has the corresponding tag. 
			\item When an entry of the page buffer fills, that particular page
				read operation is complete. 
		\end{enumerate}
	\item Page Write:
		\begin{enumerate}
			\item Assuming the data to be written already occupies an entry in the
				page buffer, the user sends the write request (\textit{sendCmd}
				method) with the associate tag of that entry. 
			\item The user monitors the \textit{getWriteDataReq} method for a tag.
				Upon receiving a tag, the user sends an entry in the page buffer
				corresponding to that tag via the \textit{writeWord} method in
				128-bit bursts.
			\item When the entire page buffer entry has been sent, the write is
				complete.
		\end{enumerate}
	\item Block Erase:
		\begin{enumerate}
			\item User sends the erase command using \textit{sendCmd} with any tag
				(Don't Care tag) and can consider the operation done. 
		\end{enumerate}
\end{itemize}
				

Note that all methods enqueue into or dequeue from FIFOs, meaning that methods
block when it is not ready, which could occur, for example, if the command
queue is full or read data is not yet available. 

The tagging scheme and page buffer is required because controller
requests are often not executed in the order it was received. This is because
internally, the flash controller contains a scoreboard that schedules each
request in a way that maximizes bus utilization (discussed in
Section~\ref{sec:scoreboard}). As a result, read data bursts corresponding to a
particular request may arrive out of order and even interleaved with data from
other requests, while write data for a particular request is not needed until
the request is scheduled to be fulfilled. Unique tags help distinguish data of
different requests. 

\section{Micron MLC NAND Chips}
Before we delve into the controller design, we first present some basic details
about the NAND chip on the flash board.  We use Micron MLC NAND chips in a
256Gbit package with 4 dies per package. Its I/O interface is shown in
Figure~\ref{fig:nandPackage}. The \textit{command} interface consists of Address Latch Enable
(ALE), Command Latch Enable (CLE), Write/Read (WR\#) and Write Protect (WP\#).
The \textit{data} interface consists of an 8-bit wide data port (DQ) and a Data
Strobe (DQS).  These signals are shared within a bus. The dies in the package
are independently selected using the Chip Enable pin (CE\#). In general, we use
the 8-bit DQ port to send and receive bursts of data. This includes page data
as well as controller requests and acknowledgments. The \textit{command}
interface latch enables are used to indicate the type of data that is being
sent on the DQ port. The details of the communication protocol is defined by
the Open NAND Flash Interface (ONFI 2.2) standard~\cite{onfi}, which specifies
timing requirements and command feature sets supported by the chip. The chip
can operate at various speeds. The fastest mode is 100MHz, with DQ transferring
at double data rate (DDR). This is also the speed of the NAND buses. 

The access latencies of the chip are 75$\mu$s for reads, 1300$\mu$s for writes and
3.8ms for erases. This means that after requesting for read data at a
particular address, the controller must wait 75$\mu$s before it can begin
transferring read data from the NAND chip. 


\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.4]{./figures/nand_package.png}
	\caption{Micron MLC NAND Package}
	\label{fig:nandPackage}
	\end{center}
\end{figure}

\section{Architecture Overview}
The architecture of the BlueFlash controller is shown in
Figure~\ref{fig:controllerArch}. Data and commands are expected to arrive from
the primary carrier Virtex-7 FPGA over serial GTX-GTP links to the flash board.
The flash controller accepts these via the user interface defined previously,
and distributes the request to multiple smaller independent bus controllers. As
the name suggests, each bus controller manages a single bus containing 8 NAND
targets that share a command (CLE, ALE, WR\#, WP\#) and data bus (DQ, DQS). The
bus controller schedules commands by consulting a scoreboard. Data to and from
the NAND chips is passed through an ECC engine for encoding and decoding.
Finally, the PHY communicates with the chips according to the timings set by
the ONFI protocol. We discuss each block in detail below from right to left in
the diagram. 

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.6]{./figures/pdf_out/top_arch-crop.pdf}
	\caption{Flash controller architecture}
	\label{fig:controllerArch}
	\end{center}
\end{figure}

\section{NAND I/O Primitives}

Similar to DRAM chips, modern NAND chips use synchronous DDR interfaces to
communicate with its controller. This interface consists of a bidirectional
data strobe (DQS) and a 8-bit wide data bus (DQ). The idea is that the sender
of a data burst toggles DQS at the same rate and aligned to the DQ bus
(Figure~\ref{fig:dqdqs}). The strobe serves as a validation indicator for the data. The
receiver can then use the strobe as a clock to capture the data. 

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.9]{./figures/pdf_out/dq-dqs-crop.pdf}
	\caption{Synchronous DDR Data (DQ) and Strobe (DQS)}
	\label{fig:dqdqs}
	\end{center}
\end{figure}

While fast, this interface presents timing challenges because factors such as
drive strength, trace length, package skew, environment conditions, capacitance
can all affect the rise and fall time of the signals. Thus sub-clock period
timing adjustments on the data strobe is required to ensure that we are
capturing valid data on the DQ data bus. We make use of hard Xilinx FPGA
primitives IDELAY, ODDR, IDDR and IOBUF to achieve this. The organization of
the NAND I/O block for the DQ and DQS path is shown in Figure~\ref{fig:nandio}. The
ODDR/IDDR primitives create
DDR bursts on the bus, while IOBUF is a simple tristate buffer to handle the
bidirectional bus. IDELAY allows for dynamic input delay adjustment from 0 to
5ns at 0.156ns increments. The delay tap value will be set by the calibration
module. 

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.7]{./figures/pdf_out/nand_io-crop.pdf}
	\caption{Controller NAND I/O Primitives}
	\label{fig:nandio}
	\end{center}
\end{figure}


Another timing challenge is the data strobe (DQS) to system clock domain
crossing. DQS may have any phase relationship with respect to the internal FPGA
system clock. Thus data captured using DQS cannot be directly used within the
FPGA. Typically for clock domain crossing, one can use synchronization
flip-flops (e.g. SyncReg in Bluespec library) with handshaking or
synchronization FIFOs (e.g. SyncFIFO). However, the former option does not work
for crossing arbitrarily phase shifted clock domains that operate at the same
frequency. The latter option also does not work because DQS is not an alway-on
clock - it only toggles when data is transmitted and cannot be used to clock
the synchronization FIFO further down the data pipeline. Instead, we sample the
DQS captured data using 4 different phase shifted FPGA system clocks at
0$^\circ$, 90$^\circ$, 180$^\circ$ and 270$^\circ$ using a set of registers as
shown in Figure~\ref{fig:nandio}. The calibration module (discussed in
Section~\ref{sec:calib})
determines the safest clock phase to use for domain transfer from DQS. We note
that extra register stages were added to ensure that all data arrive at the
same cycle and there is at least half a clock cycle of setup time ($\geq$ 5ns) when
transferring from one shifted clock domain to another. 

\section{Read Capture Calibration}
\label{sec:calib}

Because the chip timing takes on wide ranges and varies across different NAND
chips, boards and buses, a dynamic read calibration algorithm is implemented to
ensure that we capture valid data in synchronous mode. Calibration adjusts the following: 

\begin{itemize}
	\item Data to strobe delay (DQ-DQS phase)
	\item Read data capture domain crossing (DQS-to-FPGA clock)
	\item Read valid latency
\end{itemize}

The calibration algorithm works by reading a known training pattern from the NAND and
performing adjustments until the pattern is seen by the controller. Since
regular NAND pages can contain bit errors, reading patterns from them for
calibration is not a good choice. Instead, we simply read the fixed NAND parameter
page, which has enough bit pattern variations for calibration. The algorithm
works as follows:

%too much detail?
\begin{enumerate}
	\item Request read on parameter page 
	\item Using 4 FIFOs, enqueue 32 cycles of read data captured at 4 different clock phases
		(0$^\circ$, 90$^\circ$,	180$^\circ$, 270$^\circ$)
		(Figure~\ref{fig:nandio})
	\item Keep a counter while dequeuing each FIFO and examining the data. 
	\begin{itemize}
			\item	If expected data pattern is seen on any of the 4 FIFOs, the counter
	tells us the read valid latency, while the FIFO that has the first burst of
	valid data tells us the approximate phase relationship between the data and
	system clock to allow us to select the correct phase for DQS-to-FPGA clock domain crossing. 
			\item If we do not see expected data in the FIFO, the data to strobe
				alignment is wrong. Thus increment IDELAY on the DQS path and repeat Step 1. 
	\end{itemize}
\end{enumerate}

We observe that there is a chance for setup/hold time violation if we sample
the DQS clock domain data using 4 shifted FPGA clocks. For example, if the
DQS-to-FPGA clock phase relationship is 91$^\circ$, metastability can occur on
the register sampling the data using the 90$^\circ$ FPGA clock. However, we can
simply tolerate this by choosing to always read the data using the register
clocked 90$^\circ$ after observing the first valid burst of data. This way,
regardless of the outcome of the metastable sample, we always have at least
2.5ns of setup time. We note that calibration is an iterative process that may take
many cycles, but it is only done once at power on for each chip. 


\section{ONFI-compliant PHY}
The BlueFlash PHY component is a large state machine that implements a
subset of the ONFI protocol. The PHY can operate in two modes: asynchronous and
synchronous. In asynchronous mode, controller and NAND chips are independently
internally clocked, with no clock passed between them. Communication is done by
waiting a fixed length of time after toggling command or data lines. This is a
slow access mode (10MT/s) and the default power on mode. In synchronous mode, a
clock is forwarded from the controller to the NAND chip (hence they are clock
synchronous), and the strobe based DDR interface is the primary communication
method. We can achieve 200MT/s this way. 

In either mode, the PHY state machine implements 4 different communication
cycles: \textit{command}, \textit{address}, \textit{data input} and
\textit{data output}. Each type of cycle has strict timing requirements as
specified by the datasheet. An example of a synchronous data output cycle is
shown in Figure~\ref{fig:phyoutcyc}. \textit{By composing various PHY cycles together, we
form a complete operation}. For example, an erase block request from the user
is composed of 1 \textit{command} PHY cycle, 3 \textit{address} cycles and 1
more \textit{command} cycle in sequence. 

\begin{figure}[ht]
	\begin{center}
	\includegraphics[width=\textwidth]{./figures/phy_output_cycle.JPG}
	\caption{PHY Data Output Cycle}
	\label{fig:phyoutcyc}
	\end{center}
\end{figure}



\section{Bus Controller and Scoreboard}
\label{sec:scoreboard}

The primary function of the bus controller is to break down requests (such as
read page) into smaller PHY cycles (command, address, data input, data output)
and issue them to the PHY. However, to achieve high bandwidth and low response
time, chips must operate in parallel and the bus should be kept as busy as
possible. We do this by interleaving PHY cycles of different requests and
different chips on the bus. This behavior is shown in
Figure~\ref{fig:commandinterleave}.

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.8]{./figures/pdf_out/command_interleaving-crop.pdf}
	\caption{Interleaving of NAND flash commands}
	\label{fig:commandinterleave}
	\end{center}
\end{figure}

A scoreboard is used to schedule PHY cycles onto the bus and track the status of
each chip and request. The architecture of the scoreboard is shown in
Figure~\ref{fig:scoreboardarch}. We create a request FIFO for each chip since
requests aimed at a particular chip should be executed in the order it was
received (reordering is possible but requires checking for data hazards). In
addition, for each chip, we keep a busy timer counter and a stage register. Upon
receiving a request to read, write or erase, the bus
controller immediately passes the request to the scoreboard. The scoreboard
distributes the request to the appropriate chip request queue. The scheduler
works in a round robin fashion. It picks the first serviceable request from the
next chip and enqueues its next stage for execution. It then sets the busy timer to a
predefined \textit{estimate} of when the stage would be complete. Finally it
updates the stage register to indicate which stage of the request it is
currently on. It then moves to the next chip to schedule the next request. When
the busy timer expires, the scheduler queues a request for the PHY to check if
the chip has finished its job. If it has, the next stage is scheduled, otherwise
the busy timer is set once again to further wait a predefined amount of time.
When the request is complete, it is removed from the chip queue. 

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.85]{./figures/pdf_out/scoreboard-crop.pdf}
	\caption{Scoreboard architecture}
	\label{fig:scoreboardarch}
	\end{center}
\end{figure}

We make a few remarks about the scheduler. First, starvation of chips will not
occur due to round robin scheduling. Second, we can only set an estimate of when
a chip may complete its job because intrinsic NAND latencies can take on a large
range. For example, a block erase can take 3.8ms to 10ms~\cite{mlcDatasheet} to complete. We
must also poll the chip to obtain its status. The polling interval is a trade-off
between wasting bus bandwidth and response latency. Finally, we note that the
scheduler can have a notable performance impact depending on the workload. For
example, a read-heavy latency-sensitive application may benefit from a scheduler
that prioritizes short read operations over long erase and write operations. 

\section{ECC Encoder and Decoder}
\label{sec:ecc}
Bit errors are common occurrences in NAND flash chips, and error
rate is influenced by many factors such as the number of P/E cycles, the value
of the data and environment variables. ECC is a common technique to fix bit errors. 

%http://www.bswd.com/FMS13/FMS13-Klein-Alrod.pdf
%http://people.csail.mit.edu/abhiag/HLS_Comparison.pdf
%http://downloads.bbc.co.uk/rd/pubs/whp/whp-pdf-files/WHP031.pdf
Commercial SSDs today use multiple levels of DSP techniques to correct errors.
BCH codes or LDPC codes are commonly used~\cite{eccForNand}. Since we are building an
experimentation framework, long term reliability is not a priority. We chose to
use Reed-Solomon codes since it consumes low resources on the FPGA, it is a common
IP used on the FPGA and it just meets the minimum ECC requirement. Our MLC NAND
chips require error correct capabilities of 24-bits per 1080 bytes of
data~\cite{mlcDatasheet}. A common Reed-Solomon decoder is
\(RS(n,k)=RS(255,223)\) with 16B parity (6.3\%), but this configuration exceeds
flash page capacity. Each MLC flash page contains 8192B of user
data with 448B (5.2\%) provisioned for ECC parity bytes. We adjust the decoder to
be \(RS(255,243)\) with 12B parity (4.7\%). This allows us to correct 6 word and
detect 12 word errors per 255B block. This is approximately equivalent to 24-bit
ECC per 1020B of data and thus meeting the requirement. Note that this
equivalence only holds because NAND bit errors tends to be uniformly distributed
across a page (i.e. Seeing 24 bit errors bunched together is rare). We used a
modified ECC decoder from Agarwal et al.~\cite{agarwal:rs}. Details of the decoder is
beyond the scope of this thesis. The encoder block follows a standard design ~\cite{eccWhitePaper} and
is shown in Figure~\ref{fig:rsencoder}. It is fully pipelined and uses the standard
generator polynomial coefficients for \(t=6\) obtained using Matlab. 

\begin{figure}[ht]
	\begin{center}
	\includegraphics[scale=0.4]{./figures/rs_encoder.png}
	\caption{Reed Solomon encoder architecture (shrunken version with 4 word blocks)}
	\label{fig:rsencoder}
	\end{center}
\end{figure}

For each bus, we integrated two pairs of encoder-decoders in parallel since the
NAND bus is 16-bit wide (after DDR) while encoder-decoders words are 8-bits
wide. On each page reads and writes we process the data in 255B blocks (zero pad
the last block). We add large page-sized buffers before and after the encoder-decoders
because for PHY DDR write bursts to the chips, new data must be available each cycle.
Similarly on read DDR bursts, there must be space to accept the data. 

We observe that ECC can add significant latency because of: (i) pipeline delay
and (ii) if there are bit errors, the pipeline can stall for hundreds of cycles
while iterative algorithms in the decoder (i.e. Chien Search and Berlekamp)
identifies the error. This is explored in Section~\ref{sec:accesslat}. 

\section{Flash Controller}
The top level flash controller instantiates all 8 bus controllers. It forwards
commands to each bus controller and aggregates data bursts into 128-bit words for
the user or splits them for the controller. 

\section{Serializer/Deserializer}

The flash board acts as a slave storage device to the master carrier FPGA on the
Xilinx VC707 board. We use high speed GTX-GTP serial links for communication
between the primary Virtex FPGA and the flash controller on the Artix FPGA. 
The communication IP is the Xilinx provided Aurora protocol. The flash
controller's interface is "forwarded" to the primary FPGA via Aurora and the
serial links for the hardware FTL and accelerators running on the primary FPGA. 


